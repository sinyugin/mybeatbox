package catchfire.ru.mybeatbox;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс находит assets и воспроизводит их как звук.
 *
 */


public class BeatBox {

    public static final String TAG = "MyBeatBox";

    //указывает на имя папки где лежат файлы asset
    //public static final String FOLDER = "sample_sounds";
    public static final String FOLDER = "sounds";

    //задает число одновременно проигрываемых звуков
    private static final int MAX_SOUNDS = 1;
    private SoundPool mSoundPool;

    //доступ к asset через специальный менеджер
    private AssetManager mAssetManager;

    //при создании объекта передаем в качестве параметра context. Он требуется для получения
    //менеджера для управления asset. Логично) Подходит любой context.
    public BeatBox(Context context) {
        mAssetManager = context.getAssets();
        mSoundPool = new SoundPool(MAX_SOUNDS, AudioManager.STREAM_MUSIC, 0);
        loadSounds();
    }

    //Метод воспроизводит аудио
    public void play(Sound sound){
        Integer soundId = sound.getSoundId();
        if (soundId == null){
            return;
        }
        mSoundPool.play(soundId, 1.0f, 1.0f, 1, 0, 1.0f);
    }

    public void release(){
        mSoundPool.release();
    }

    //получаем список звуков
    private List<Sound> mSounds = new ArrayList<>();

    private void loadSounds() {
        String[] sounds = new String[]{};
        try {
            //метод list(String folderName) отражает все что есть в указанной папке
            sounds = mAssetManager.list(FOLDER);
            //Log.d(TAG, "We found " + sounds.length + " sound files");
        } catch (IOException e) {
            //Log.e(TAG, "We haven't found any sound files");
            e.printStackTrace();
        }
        //Парсим массив с именами sound файлов. Добавляем их в ArrayList
        for (String filename: sounds) {
            try {
                //Загружаем все аудио в BeatBox
                String assetPath = FOLDER + "/" + filename;
                Sound sound = new Sound(assetPath);
                load(sound);
                mSounds.add(sound);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //загружаем звук в SoundPool
    private void load(Sound sound) throws IOException {
        AssetFileDescriptor afd = mAssetManager.openFd(sound.getAssetPath());
        int soundId = mSoundPool.load(afd, 1);
        sound.setSoundId(soundId);
    }

    //гетер
    public List<Sound> getSounds() {
        return mSounds;
    }
}
