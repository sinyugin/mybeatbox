package catchfire.ru.mybeatbox;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

/**
 * Created by sinyu on 05.10.2017.
 */

public class SoundViewModel extends BaseObservable {

    //2 properties are the interface's adapter. Adapter will use them
    private Sound mSound;
    private BeatBox mBeatBox;

    public SoundViewModel(BeatBox beatBox) {
        mBeatBox = beatBox;
    }

    public Sound getSound() {
        return mSound;
    }

    //This is method is required for setting a button title in layout
    @Bindable
    public String getTitle(){
        return mSound.getName();
    }

    public void setSound(Sound sound) {
        mSound = sound;
        notifyChange();
    }


    public void onButtonClicked() {
        mBeatBox.play(mSound);
    }
}
