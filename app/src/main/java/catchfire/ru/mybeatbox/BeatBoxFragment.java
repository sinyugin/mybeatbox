package catchfire.ru.mybeatbox;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import catchfire.ru.mybeatbox.databinding.FragmentBeatBoxBinding;
import catchfire.ru.mybeatbox.databinding.ListItemSoundBinding;

/**
 * Created by sinyu on 04.10.2017.
 */

public class BeatBoxFragment extends Fragment {


    //Создаем объект BeatBox который хранит список с аудио файлами
    private BeatBox mBeatBox;

    public static BeatBoxFragment newInstance(){
        return new BeatBoxFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mBeatBox = new BeatBox(getActivity());
    }

    //Связываем view из layout используя сгенерированный класс FragmentBeatBoxBinding
    //Получаем ссылку на inflated view с помощью метода getRoot()
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        FragmentBeatBoxBinding binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_beat_box,
                container,
                false);

        //Подвязывем RV
        binding.recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        //Подвязываем адаптер для RV. Адаптер использует паттерн ViewHolder. Соответственно
        //ViewHolder будет представлением. Класс ViewHolder также получен с помощью DataBinding
        //В адаптер прилитает список звуков из BeatBox.class
        binding.recyclerView.setAdapter(new SoundAdapter(mBeatBox.getSounds()));

        return binding.getRoot();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mBeatBox.release();
    }

    //Создаем класс для SoundHolder
    private class SoundHolder extends RecyclerView.ViewHolder{

        //экземпляр класса для связывания данных
        private ListItemSoundBinding mSoundBinding;

        private SoundHolder(ListItemSoundBinding binding){
            super(binding.getRoot());
            mSoundBinding = binding;

            //construct and attach your view model
            mSoundBinding.setViewModel(new SoundViewModel(mBeatBox));
        }

        public void bind(Sound sound){
            // hooking up ViewModel. Update the data that view model is working with
            mSoundBinding.getViewModel().setSound(sound);
            // hooking up ViewModel
            // updates views at a very high speed. Force the layout to immediately update itself
            mSoundBinding.executePendingBindings();
        }
    }

    //Связывает layout list_item_sound с SoundHolder
    private class SoundAdapter extends RecyclerView.Adapter<SoundHolder>{

        @Override
        public SoundHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            ListItemSoundBinding binding = DataBindingUtil.inflate(
                    inflater,
                    R.layout.list_item_sound,
                    parent,
                    false
            );
            return new SoundHolder(binding);
        }

        // Связываем адаптер со списком звуков
        private List<Sound> mSounds;
        public SoundAdapter(List<Sound> sounds) {
            mSounds = sounds;
        }

        @Override
        public int getItemCount() {
            return mSounds.size();
        }

        @Override
        public void onBindViewHolder(SoundHolder holder, int position) {
            //finish hooking up ViewModel
            Sound sound = mSounds.get(position);
            holder.bind(sound);
        }
    }
}
