package catchfire.ru.mybeatbox;

/**
 * Created by sinyu on 29.10.2017.
 */

public class Pic {

    private String mName;
    private String mAssetPath;

    //By making mSoundId an Integer instead of an int, you make it possible to say that
    // a Sound has no value set for mSoundId by assigning it a null value.
    private Integer mPicId;

    public Pic(String assetPath) {
        mAssetPath = assetPath;
        String[] components = assetPath.split("/");
        String filename = components[components.length-1];
        mName = filename.replace(".png", "");
    }

    public String getAssetPath() {
        return mAssetPath;
    }

    public String getName() {
        return mName;
    }

    public Integer getPicId() {
        return mPicId;
    }

    public void setPicId(Integer picId) {
        mPicId = picId;
    }

}
