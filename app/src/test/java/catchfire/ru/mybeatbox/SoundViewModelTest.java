package catchfire.ru.mybeatbox;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * С помощью этого теста можно протестировать
 */
public class SoundViewModelTest {


    private SoundViewModel mSubject;
    private Sound mSound;
    private BeatBox mBeatBox;

    @Before
    public void setUp(){
        mSound = new Sound("assetPath");
        mBeatBox = mock(BeatBox.class);
        mSubject = new SoundViewModel(mBeatBox);
        mSubject.setSound(mSound);
    }

    @Test
    public void testSoundViewModel(){
        assertThat(mSubject.getTitle(), is(mSound.getName()));
    }

    @Test
    public void callsBeatBoxPlayOnButtonClicked(){
        mSubject.onButtonClicked();
        verify(mBeatBox).play(mSound);
        // equals to
        // verify(mBeatBox);
        // mBeatBox.play(mSound);
    }
}